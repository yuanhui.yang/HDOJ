# HDOJ

Hangzhou Dianzi University Online Judge

| # | Title | Solution |
| --- | --- | --- |
| [1711](http://acm.hdu.edu.cn/showproblem.php?pid=1711) | [Number Sequence](http://acm.hdu.edu.cn/showproblem.php?pid=1711) | [C++](https://github.com/yuanhui-yang/HDOJ/blob/master/1711.cpp) |