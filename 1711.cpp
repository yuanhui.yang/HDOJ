/*
Number Sequence
http://acm.hdu.edu.cn/showproblem.php?pid=1711

Time Limit: 10000/5000 MS (Java/Others)    Memory Limit: 32768/32768 K (Java/Others)
Total Submission(s): 26562    Accepted Submission(s): 11206


Problem Description
Given two sequences of numbers : a[1], a[2], ...... , a[N], and b[1], b[2], ...... , b[M] (1 <= M <= 10000, 1 <= N <= 1000000). Your task is to find a number K which make a[K] = b[1], a[K + 1] = b[2], ...... , a[K + M - 1] = b[M]. If there are more than one K exist, output the smallest one.
 

Input
The first line of input is a number T which indicate the number of cases. Each case contains three lines. The first line is two numbers N and M (1 <= M <= 10000, 1 <= N <= 1000000). The second line contains N integers which indicate a[1], a[2], ...... , a[N]. The third line contains M integers which indicate b[1], b[2], ...... , b[M]. All integers are in the range of [-1000000, 1000000].
 

Output
For each test case, you should output one line which only contain K described above. If no such K exists, output -1 instead.
 

Sample Input
2
13 5
1 2 1 2 3 1 2 3 1 3 2 1 2
1 2 3 1 3
13 5
1 2 1 2 3 1 2 3 1 3 2 1 2
1 2 3 2 1
 

Sample Output
6
-1
 

Source
HDU 2007-Spring Programming Contest
*/

#include <iostream> // std::cout; std::cin
#include <fstream> // std::fstream::open; std::fstream::close;
#include <ctime>
#include <cstdlib> // rand
#include <cassert> // assert
#include <cctype> // isalnum; isalpha; isdigit; islower; isupper; isspace; tolower; toupper
#include <cmath> // pow; sqrt; round; fabs; abs; log
#include <climits> // INT_MIN; INT_MAX; LLONG_MIN; LLONG_MAX; ULLONG_MAX
#include <cfloat> // DBL_EPSILON; LDBL_EPSILON
#include <cstring> // std::memset
#include <algorithm> // std::swap; std::max; std::min; std::min_element; std::max_element; std::minmax_element; std::next_permutation; std::prev_permutation; std::nth_element; std::sort; std::lower_bound; std::upper_bound; std::reverse
#include <limits> // std::numeric_limits<int>::min; std::numeric_limits<int>::max; std::numeric_limits<double>::epsilon; std::numeric_limits<long double>::epsilon;
#include <numeric> // std::accumulate; std::iota
#include <string> // std::to_string; std::string::npos; std::stoul; std::stoull; std::stoi; std::stol; std::stoll; std::stof; std::stod; std::stold; 
#include <list> // std::list::merge; std::list::splice; std::list::merge; std::list::unique; std::list::sort
#include <bitset>
#include <vector>
#include <deque>
#include <stack> // std::stack::top; std::stack::pop; std::stack::push
#include <queue> // std::queue::front; std::queue::back; std::queue::pop; std::queue::push; std::priority_queue; std::priority_queue::top; std::priority_queue::push; std::priority_queue::pop
#include <set> // std::set::count; std::set::find; std::set::equal_range; std::set::lower_bound; std::set::upper_bound
#include <map> // std::map::count; std::map::find; std::map::equal_range; std::map::lower_bound; std::map::upper_bound
#include <unordered_set>
#include <unordered_map>
#include <utility> // std::pair; std::make_pair
#include <iterator>
#include <functional> // std::less<int>; std::greater<int>
using namespace std;

// BEGIN: http://acm.hdu.edu.cn/discuss/problem/post/reply.php?postid=31093&messageid=1
// BEGIN: http://acm.hdu.edu.cn/discuss/problem/post/reply.php?postid=27625&messageid=1
class Solution {
public:
	int numberSequence(vector<int>& T, vector<int>& P) {
		if (T.size() == P.size()) {
			return T == P ? 0 : -1;
		}
		if (T.size() < P.size()) {
			return -1;
		}
		if (P.empty()) {
			return 0;
		}
		vector<int> pi(P.size() + 1);
		pi.front() = -1;
		int i = 0, j = -1, m = T.size(), n = P.size();
		while (i < n) {
			if (j == -1 or P.at(i) == P.at(j)) {
				pi.at(++i) = ++j;
			}
			else {
				j = pi.at(j);
			}
		}
		i = 0;
		j = 0;
		while (i < m) {
			if (j == -1 or T.at(i) == P.at(j)) {
				i++;
				j++;
			}
			else {
				j = pi.at(j);
			}
			if (j == n) {
				return i + 1 - j;
			}
		}
		return -1;
	}
};
// END: http://acm.hdu.edu.cn/discuss/problem/post/reply.php?postid=27625&messageid=1
// END: http://acm.hdu.edu.cn/discuss/problem/post/reply.php?postid=31093&messageid=1

// BEGIN: http://acm.hdu.edu.cn/discuss/problem/post/reply.php?postid=19997&messageid=1
// class Solution {
// public:
// 	int numberSequence(vector<int>& T, vector<int>& P) {
// 		if (T.size() == P.size()) {
// 			return T == P ? 0 : -1;
// 		}
// 		if (T.size() < P.size()) {
// 			return -1;
// 		}
// 		if (P.empty()) {
// 			return 0;
// 		}
// 		vector<int> pi(P.size(), 0);
// 		for (size_t i = 1, j = 0; i < pi.size(); i++) {
// 			while (j > 0 and P.at(i) != P.at(j)) {
// 				j = pi.at(j - 1);
// 			}
// 			if (P.at(i) == P.at(j)) {
// 				pi.at(i) = ++j;
// 			}
// 		}
// 		for (size_t i = 0, j = 0; i < T.size(); i++) {
// 			while (j > 0 and T.at(i) != P.at(j)) {
// 				j = pi.at(j - 1);
// 			}
// 			if (T.at(i) == P.at(j)) {
// 				j++;
// 			}
// 			if (j == P.size()) {
// 				return i + 2 - P.size();
// 			}
// 		}
// 		return -1;
// 	}
// };
// END: http://acm.hdu.edu.cn/discuss/problem/post/reply.php?postid=19997&messageid=1

int main(void) {
	Solution solution;
	int N;
	scanf("%d", &N);
	for (int i = 0; i < N; i++) {
		int t, p;
		scanf("%d", &t);
		scanf("%d", &p);
		vector<int> T, P;
		for (int j = 0; j < t; j++) {
			int x;
			scanf("%d", &x);
			T.push_back(x);
		}
		for (int j = 0; j < p; j++) {
			int x;
			scanf("%d", &x);
			P.push_back(x);
		}
		printf("%d\n", solution.numberSequence(T, P));
	}
	return 0;
}